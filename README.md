# Dynamics Learning Code

Code related with the benchmarking of different dynamics learning methods. Also, preliminary (and small) versions of the data set are included in the Dataset folder.

# Data set

## GP torque controls

dataset_v01.npz - second version of the dataset. 10 sequences with 5000 samples each (each sequence corresponds to 5 seconds of execution). The control trajectories were sampled from two different controllers (i.e., one GP was used to generated the first half of the sequences and another one for the rest). This file is included as a reference only; i.e., to ilustre the format and the actual recorded information from the robot platform. Notice that the name of the fields in this file was updated to match the API of the robot platform (commit hash: 2c95a6c4).

~~dataset_v02.npz~~. The whole data set is not longer available, please make sure to delete your copies to avoid mistakes with the evaluation.

### Training and validation sets

[dataset_v02_training.npz](https://owncloud.tuebingen.mpg.de/index.php/s/Ytzi69HdsaPsw5p/download). (with collisions) 152 sequences with 19000 samples each (19 seconds of execution for each rollout). There are 8 trajectories for each controller (each controller corresponds to a different GP). The data set can be downloaded from [here](https://owncloud.tuebingen.mpg.de/index.php/s/Ytzi69HdsaPsw5p/download).

[dataset_v03_training.npz](https://owncloud.tuebingen.mpg.de/index.php/s/CYrWAX4k3XnF5n8/download). This data set **does not have collisions** and should be the definite version. 152 sequences with 19000 samples each (19 seconds of execution for each rollout). There are 8 trajectories for each controller (each controller corresponds to a different GP). The data set can be downloaded from [here](https://owncloud.tuebingen.mpg.de/index.php/s/CYrWAX4k3XnF5n8/download).

[dataset_v04_training.npz](https://owncloud.tuebingen.mpg.de/index.php/s/HkwWjm2ApNSSRx8/download), [dataset_v04_validation.npz](https://owncloud.tuebingen.mpg.de/index.php/s/8PdrcsW4c6fqA6J/download). Explicit splitting of dataset_v03_training.npz (**no collisions**) into training and validation. This allows all the methods to perform hyperparameter optimization in a similar way. In the validation set there is a rollout for each controller present in the training data.

Please note that at testing time the training data will be available. This is useful to compute the normalization statistics for instance.

### Test sets

Make sure to not perform any kind of learning or hyperparameter optimization using these sets. 

[dataset_v04_test_iid.npz](https://owncloud.tuebingen.mpg.de/index.php/s/LS9x77sB6jENmCX/download). There are 2 rollouts for each controller present in the dataset_v04_training.npz of length 19000.

[dataset_v04_test_transfer.npz](https://owncloud.tuebingen.mpg.de/index.php/s/Q4PTeHd6t48KZaq/download). There are in total 10 unseen controllers in this set and 10 rollouts with 19000 samples for each of them.

## Sine waves position control

This dataset was generated using sine waves of the form $y(t) = A \sin(\omega t + \phi)$ to generate trajectories that were subsequently tracked by the finger using PD position control.
The dataset `dataset_sine_full.npz` contains movements over the entire task space (constrained by the range of safe angle configurations). In contrast, `dataset_sine_left.npz` only contains
movements performed in the left half of the task space. The following splits are performed using these two datasets as inputs.

* Split 1: `dataset_sine_left.npz` is used for training/validation and `dataset_sine_full.npz` is used for testing.
* Split 2: Only `dataset_sine_full.npz` is used for this split. For training, we use sine trajectores share the same phase $\phi$ across degrees of freedom while for testing trajectories with independet phases are used instead. In both cases the phases are uniformly sampled for each rollout. 
* Split 3: Only `dataset_sine_full.npz` is used for this split. For training, sine trajectories with low angular frequency are used and the rest of the angular frequencies are used for testing.

The data sets can be downloaded from [here](https://owncloud.tuebingen.mpg.de/index.php/s/mCPwHQfyp4Dejm3). More details about the splitting can be found in the script `data_extractor.py`.

For all the splits, there is a correspoding file for training (e.g., `sine_dataset_training_split_3.npz`), validation (e.g., `sine_dataset_validation_split_3.npz`), iid testing (e.g., `sine_dataset_test_iid_split_3.npz`) and transfer testing (e.g., `sine_dataset_test_transfer_split_3.npz`)

## Joint Dataset: PD control for Sine waves + GP torque controls.

The sine waves for PD controls are generated randomly for the left part of the state space and for the full task space. The angular frequencies are likewise generated for a low and for a high regime.
For training we use the data captured in the left part of the task space under low frequencies: ` dataset_v06_sines_training.npz` and `dataset_v06_sines_validation.npz`.
We have four testing settings: `dataset_v06_sines_test_iid.npz` (left side, low frequency),  `dataset_v06_sines_test_transfer_1.npz` (left side, high frequency), `dataset_v06_sines_test_transfer_2.npz` (full range, low frequency) and `dataset_v06_sines_test_transfer_3.npz` (full range, high frequency).

The GP torque controls are sampled for low and high values of lengthscale and low and high values of kernel variance. The idea is to use the data set recorded using sine waves for learning `dataset_v06_GPs_training.npz`, `dataset_v06_GPs_validation.npz` and iid testing `dataset_v06_GPs_test_iid.npz`.
The GP-generated data set is used for testing under transfer: `dataset_v06_GPs_test_transfer.npz`.

All the data set files can be found [here](https://owncloud.tuebingen.mpg.de/index.php/s/AAfdjfjc6cZYQkz). The dataset is also accessible from the cluster in `/agbs/dynlearning/Data`.


# EQL dynamics learner

### Installation:

We recommend to install `pipenv` package that manages the environment.

We included Pipfile with the requirements needed
to install eql dynamics part of the project.

Use `pipenv install` from the core folder.

This command will create new env with the same name as the folder name
and install all the dependencies from `Pipfile`.

You can activate this env using `pipenv shell` from the root folder.

# Evaluation

To evaluate a particular method with a particular dataset you can execute as a script the module DL.evaluation.evaluation. Please use as a refernce the `Evaluation.ipynb` notebook.

## Error Files

The evaluation is ultimately performed using a file containing the errors computed while executing the evaluation script. Error files for some of the methods can be found in this [link](https://owncloud.tuebingen.mpg.de/index.php/s/FwNzTwa6FwRtwtp). Feel free to add your own error files in new directories.