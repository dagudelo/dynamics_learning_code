# %% markdown
# # Results for NNs, SVGPR, delta 0 baseline and linear models for the PD sine data.
# %%
import numpy as np
import os
import matplotlib.pyplot as plt
from DL.plotting.box_plots import plot_errors


def get_diego_index(prediction_horizon,
                    history_length,
                    averaging):
    prediction_horizons = [1, 10, 100, 1000]
    history_lengths = [1, 10]

    count = 0
    for current_prediction_horizon in prediction_horizons:
        for current_history_length in history_lengths:
            for current_averaging in [True, False]:
                if prediction_horizon == current_prediction_horizon and \
                        history_length == current_history_length and \
                        averaging == current_averaging:
                    return count

                count += 1
    return np.nan


def path_to_error_file(method_name,
                       experiment_name,
                       prediction_horizon,
                       history_length):
    if experiment_name == 'sine':
        path_to_results = "/agbs/dynlearning/Errors/new_datasets/SinePD/"
    else:
        raise NotImplementedError

    if method_name == 'avg-NN':
        error_file_name = "NN/averaging_prediction_horizon_{}_history_length" \
                          "_{}_epochs_40/errors.npz".format(prediction_horizon,
                                                            history_length)
    elif method_name == 'NN':
        error_file_name = "NN/prediction_horizon_{}_history_length" \
                          "_{}_epochs_40/errors.npz".format(prediction_horizon,
                                                            history_length)
    elif method_name == 'delta 0':
        error_file_name = 'delta_0/errors_sine_pd_delta_0_{:03d}.npz'.format(
            get_diego_index(prediction_horizon=prediction_horizon,
                            history_length=history_length,
                            averaging=False))
    elif method_name == 'svgpr':
        error_file_name = 'svgpr/errors_sine_pd_svgpr_{:03d}.npz'.format(
            get_diego_index(prediction_horizon=prediction_horizon,
                            history_length=history_length,
                            averaging=False))
    elif method_name == 'avg-svgpr':
        error_file_name = 'svgpr/errors_sine_pd_svgpr_{:03d}.npz'.format(
            get_diego_index(prediction_horizon=prediction_horizon,
                            history_length=history_length,
                            averaging=True))
    elif method_name == 'linear':
        error_file_name = 'linear_model_learning_rate_0.0001/errors_sine' \
                          '_pd_linear_model_{:03d}.npz'.format(
            get_diego_index(prediction_horizon=prediction_horizon,
                            history_length=history_length,
                            averaging=False))
    elif method_name == 'avg-linear':
        error_file_name = 'linear_model_learning_rate_0.0001/errors_sine' \
                          '_pd_linear_model_{:03d}.npz'.format(
            get_diego_index(prediction_horizon=prediction_horizon,
                            history_length=history_length,
                            averaging=True))
    elif method_name in ['sys id cad', 'sys id ls', 'sys id ls-lmi']:
        identification_method = method_name[7:]
        error_file_name = 'system_id/system_id__' + experiment_name + \
                          '__horizon_' + str(prediction_horizon).zfill(4) + \
                          '__history_' + str(history_length).zfill(2) + \
                          '__identification_method_' + identification_method + \
                          '.npz'
    else:
        assert (False)

    return os.path.join(path_to_results, error_file_name)


def plot_and_save(prediction_horizon,
                  history_length,
                  experiment_name,
                  evaluations,
                  show=True):
    method_names = ['delta 0', 'avg-NN', 'avg-svgpr', 'avg-linear',
                    'NN', 'svgpr', 'linear', 'sys id cad', 'sys id ls']

    if experiment_name == 'sine':
        path_to_plots = "/agbs/dynlearning/Plots/new_datasets/SinePD/"
    else:
        raise NotImplementedError

    paths = [path_to_error_file(method_name=method_name,
                                experiment_name=experiment_name,
                                prediction_horizon=prediction_horizon,
                                history_length=history_length)
             for method_name in method_names]
    fig_violin = plot_errors(error_files=paths,
                             names=method_names,
                             violinplot=True,
                             setups=evaluations,
                             show=show)

    fig_violin.savefig(os.path.join(
        path_to_plots,
        "violin_prediction_horizon_{}_history_length_{}.pdf".format(
            prediction_horizon,
            history_length)), format="pdf")
    fig_violin.savefig(os.path.join(
        path_to_plots,
        "violin_prediction_horizon_{}_history_length_{}.png".format(
            prediction_horizon,
            history_length)))


for prediction_horizon in [1, 10, 100, 1000]:
    for history_length in [1, 10]:
        plot_and_save(prediction_horizon=prediction_horizon,
                      history_length=history_length,
                      experiment_name='sine',
                      evaluations=['training_data',
                                   'validation_data',
                                   'iid_test_data',
                                   'transfer_test_data_1',
                                   'transfer_test_data_2',
                                   'transfer_test_data_3'],
                      show=False)


exit()


# %%
prediction_horizons = [1, 10, 100, 1000]
history_lengths = [1, 10]
count = 0
names = ['avg-NN', 'avg-svgpr', 'avg-linear', 'delta 0', 'NN', 'svgpr',
         'linear', 'delta 0']
path_to_results = "/agbs/dynlearning/Errors/new_datasets/GP/"
path_to_plots = "/agbs/dynlearning/Plots/new_datasets/GP/"
print(os.path.isdir(path_to_results))
for i, prediction_horizon in enumerate(prediction_horizons):
    for j, history_length in enumerate(history_lengths):
        error_files = []
        for averaging in [True, False]:
            nn_address_new = "NN/{}prediction_horizon_{}_history_length_{}_epochs_40/errors_new.npz".format(
                "averaging_" if averaging else "",
                prediction_horizon,
                history_length)
            delta_0_address = 'delta_0/errors_GP_delta_0_{:03d}.npz'.format(
                count)
            svgpr_address = 'svgpr/errors_GP_svgpr_{:03d}.npz'.format(count)
            linear_sgd_address = 'linear_model_learning_rate_0.0001_epochs_1/errors_GP_linear_model_{:03d}.npz'.format(
                count)
            addresses = [nn_address_new, svgpr_address, linear_sgd_address,
                         delta_0_address]
            addresses = [os.path.join(path_to_results, address) for address in
                         addresses]
            error_files.extend(addresses)
            count += 1
        print("Prediction Horizon: {}, History length: {}".format(
            prediction_horizon, history_length))

        # Removing the last delta 0 from the plots
        fig_box = plot_errors(error_files[:-1], names[:-1], False)
        fig_box.savefig(os.path.join(path_to_plots,
                                     "box_plot_prediction_horizon_{}_history_length_{}.png".format(
                                         prediction_horizon, history_length)))
        # fig_violin = plot_errors(error_files[:-1], names[:-1], True)
        # fig_violin.savefig(os.path.join(path_to_plots, "violin_prediction_horizon_{}_history_length_{}.pdf".format(prediction_horizon, history_length)), format = "pdf")
        # fig_violin.savefig(os.path.join(path_to_plots, "violin_prediction_horizon_{}_history_length_{}.png".format(prediction_horizon, history_length)))
