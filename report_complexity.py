# %% markdown
# # Violin plot for the SinePD data.
# %%
import sys
import traceback

import numpy as np
import os
import pandas as pd
import matplotlib.pyplot as plt
import itertools
import matplotlib

matplotlib.rcParams.update({'errorbar.capsize': 3})

from DL.plotting.plots import box_violin_plot, aggregated_plot
from DL.utils.plot_utils import get_diego_index, path_to_error_file, \
    aggregate_RMSE, get_number_of_parameters

from DL.evaluation.evaluation import get_evaluation_errors
from report import Errors
import ipdb




if __name__ == '__main__':
    try:
        errors = Errors()



        prediction_horizons = [1, 10, 100, 1000]
        error_types = ['angle', 'velocity', 'torque']
        methods = ["NN_lr_0.0001_reg_0.0001_l_{}_w_{}".format(n, m) for (m, n) in itertools.product([64, 128, 256, 512], [2,3,4])]
        # [get_number_of_parameters(n,m, prediction_horizon=1000, history_length=10) for (m,n) in itertools.product([64, 128, 256, 512],[2,3,4])]
        path_to_plots = '/agbs/dynlearning/Plots/new_datasets/SinePD/complexity_plots/h1'

        # path_to_plots = '/is/ei/mwuthrich/local'

        for prediction_horizon in prediction_horizons:
            for error_type in error_types:
                fig, ax = errors.create_paper_plot(
                    method_names=methods,
                    prediction_horizon=prediction_horizon,
                    history_lengths=[1],
                    error_type=error_type,
                    test_dataset_names=['training_data',
                                        'iid_test_data',
                                        'transfer_test_data_3'],
                    ordered_by=None)

                filename = error_type + '__horizon_' + \
                           str(prediction_horizon).zfill(4)


                fig.savefig(os.path.join(path_to_plots, filename + '.pdf'),
                            format='pdf')
                fig.savefig(os.path.join(path_to_plots, filename + '.png'))

                    #
                    # fig.show()
                    #
                    # ipdb.set_trace()



    except:
        traceback.print_exc(sys.stdout)
        _, _, tb = sys.exc_info()
        ipdb.post_mortem(tb)
