import json
import argparse
import os
import numpy as np

from DL.utils.data_splitting import loadRobotData
from DL.methods.eql_dynamics_learner import EQL
from DL.evaluation.evaluation import evaluate

# if __name__ == "__main__":
parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument("--settings", required=True,
            help="<Required> Path to the settings file.")

args = parser.parse_args()

with open(args.settings, 'r') as f:
    params = json.load(f)
params
np.random.seed(42)
history_length = 1
prediction_horizon = 1
# history_length = params["history_length"]
# prediction_horizon = params["prediction_horizon"]
observation_sequences, action_sequences = loadRobotData(params['data'])
N = observation_sequences.shape[0]
indx = np.random.permutation(N)
validation_indx = indx[round(N*0.8):]
train_indx = indx[:round(N*0.8)]
train_observation_sequences = observation_sequences[train_indx, :]
train_action_sequences = action_sequences[train_indx, :]
val_observation_sequences = observation_sequences[validation_indx,:]
val_action_sequences = action_sequences[validation_indx,:]

if params['mode'] == 'train':
    eql = EQL(history_length=history_length,
              prediction_horizon=prediction_horizon,
              model_arch_params=params["model_arch_params"],
              model_train_params=params["model_train_params"],
              optional_params=params["optional_params"])
    eql.learn(train_observation_sequences, train_action_sequences)
    eql.save(params['model_file'])
    file_name = "./Results/errors/EQL_history_length_{4}_prediction_horizon_{5}_n_{0}_m_{1}_reg_scale_{3}_Adam_lr_{2}_bs_512".format(eql.num_h_layers,
                                                                                           eql.layer_width,
                                                                                           eql.reg_scales[-1],
                                                                                           eql.learning_rate,
                                                                                           eql.history_length,
                                                                                           eql.prediction_horizon)
    # file_name = "./Results/errors/eql_example"
    val_dataset_name = "__val_training_dataset_v3"
    train_dataset_name = "__training_dataset_v3"
    errors_val = evaluate(eql, val_observation_sequences, val_action_sequences, val_dataset_name)
    eql.new_data = True
    errors_train = evaluate(eql, train_observation_sequences, train_action_sequences, train_dataset_name)
    errors = {'training_data':errors_train, 'validation_data': errors_val}
    np.savez(file_name, **errors)

elif params['mode'] == 'eval':
    eql = EQL(history_length=history_length,
              prediction_horizon=prediction_horizon,
              model_arch_params=params["model_arch_params"],
              model_train_params=params["model_train_params"],
              optional_params=params["optional_params"])
    eql.load_normalization_stats(train_observation_sequences, train_action_sequences)
    eql.load(params['model_file'])
    file_name = "./Results/errors/test_EQL_history_length_{4}_prediction_horizon_{5}_n_{0}_m_{1}_reg_scale_{3}_Adam_lr_{2}_bs_512".format(eql.num_h_layers,
                                                                                           eql.layer_width,
                                                                                           eql.reg_scales[-1],
                                                                                           eql.learning_rate,
                                                                                           eql.history_length,
                                                                                           eql.prediction_horizon)
    # file_name = "./Results/errors/eql_example"
    val_dataset_name = "__val_training_dataset_v3"
    train_dataset_name = "__training_dataset_v3"
    eql.model_fn.set_reg_scale(eql.reg_scales[-1])
    errors_val = evaluate(eql, val_observation_sequences, val_action_sequences, val_dataset_name)
    eql.new_data = True
    errors_train = evaluate(eql, train_observation_sequences, train_action_sequences, train_dataset_name)
    errors = {'training_data':errors_train, 'validation_data': errors_val}
    np.savez(file_name, **errors)
