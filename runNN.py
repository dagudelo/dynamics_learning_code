import json
import argparse
import os
import numpy as np

from DL.utils.data_splitting import loadRobotData
from DL.methods.nn_dynamics_learner import NNDynamicsLearner
from DL.evaluation.evaluation import evaluate


# if __name__ == "__main__":
parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument("--settings", required=True,
            help="<Required> Path to the settings file.")

args = parser.parse_args()
with open(args.settings, 'r') as f:
    params = json.load(f)

history_length = params["history_length"]
prediction_horizon = params["prediction_horizon"]

train_observation_sequences, train_action_sequences = loadRobotData(params['train_data'])
val_observation_sequences, val_action_sequences = loadRobotData(params['val_data'])

nn = NNDynamicsLearner(history_length=history_length,
                       prediction_horizon=prediction_horizon,
                       model_arch_params=params["model_arch_params"],
                       model_train_params=params["model_train_params"],
                       mode=params['mode'])
if params['mode'] == 'train':
    nn.learn(train_observation_sequences, train_action_sequences)
    nn.save(params['model_file'])
elif params['mode'] == 'eval':
    nn.load_normalization_stats(train_observation_sequences, train_action_sequences)
    nn.load(params['model_file'])
file_name = "./Results/errors/history_length_{4}_prediction_horizon_{5}__NN_n_{0}_m_{1}_l2_reg_{3}_Adam_lr_{2}_bs_512_epochs_400".format(nn.num_layers,
                                                                         nn.size,
                                                                         nn.learning_rate,
                                                                         nn.l2_reg,
                                                                         nn.history_length,
                                                                         nn.prediction_horizon)
val_dataset_name = 'validation_data'
train_dataset_name = 'training_data'
errors_val = evaluate(nn, val_observation_sequences, val_action_sequences, val_dataset_name)
errors_train = evaluate(nn, train_observation_sequences, train_action_sequences, train_dataset_name)
errors = {train_dataset_name : errors_train, val_dataset_name : errors_val}
np.savez(file_name, **errors)
