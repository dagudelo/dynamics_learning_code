"""
Using an available python implementation of PILCO to learn dynamics.
Only the funcionality related with the forward model learning is being
used. Download a (forked) version of it at https://github.com/DiegoAE/PILCO.
"""

import argparse
from collections import defaultdict
import gpflow
import numpy as np
import tensorflow as tf
from DL.utils.data_loading import loadRobotData
from DL.methods.pilco_dynamics_learner import PilcoDynamicsLearner


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("--mode", choices=['train', 'eval'], default='train')
    parser.add_argument("--input", required=True,
            help="<Required> Path to the input file. Could be either a dataset"
            "or a trained model depending on the mode.")
    parser.add_argument("--ntraining", type=int,
            help="Number of training points that will be sampled"
            " for training")
    parser.add_argument("--ninducing", type=int,
            help="Number of inducing points")
    parser.add_argument("--output", help="Path to save the trained model")
    args = parser.parse_args()
    if args.mode == 'eval' and (args.ntraining or args.ninducing or \
            args.output):
        parser.error('Invalid arguments under the eval mode.')
    if args.mode == 'train' and not(args.input and args.output):
        parser.error('Missing required arguments under train mode.')
    input_data = args.input
    if args.mode == 'train':
        session = gpflow.session_manager.get_session()
        print("DEVICES: ", session.list_devices())
        print("AVAILABLE: ", tf.test.is_gpu_available())

        pilco_dyn = PilcoDynamicsLearner(1, 1, args.ninducing, args.ntraining)
        observations_seqs, actions_seqs = loadRobotData(args.input)
        pilco_dyn.learn(observations_seqs, actions_seqs)
        pilco_dyn.save(args.output)
    elif args.mode == 'eval':
        pass  # TODO
