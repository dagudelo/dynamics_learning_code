#

"""
Script to test functionality of SKI implementation
"""

import numpy as np
import torch
import math
from DL.utils.data_loading import loadRobotData

from DL.methods.SKI import SKIDynamicsLearner

from DL.utils.standardizer import Standardizer

# easy test example
#n = 40
#train_x = torch.zeros(pow(n, 2), 2)
#for i in range(n):
#    for j in range(n):
#        train_x[i * n + j][0] = float(i) / (n-1)
#        train_x[i * n + j][1] = float(j) / (n-1)
## True function is sin( 2*pi*(x0+x1))
#train_y = torch.sin((train_x[:, 0] + train_x[:, 1]) * (2 * math.pi)) + torch.randn_like(train_x[:, 0]).mul(0.01)
#train_y = train_y.reshape([-1, 1])
#
#
#
#trainXNumpy = np.asarray(train_x)
#trainYNumpy = np.asarray(train_y)
#
#xStandardizer = Standardizer(trainXNumpy)
#yStandardizer = Standardizer(trainYNumpy)
#
#xStded = xStandardizer.standardize(trainXNumpy)
#trainX2 = torch.tensor(xStded)
#yStded = yStandardizer.standardize(trainYNumpy)
#trainY2 = torch.tensor(yStded)
#
#
#trainX = torch.tensor(trainXNumpy)
#trainY = torch.tensor(trainYNumpy)
#
#model = SKIDynamicsLearner(state_dims=2,
#                           action_dims=0,
#                           learningRate=.1,
#                           trainingIterations=1000)
#model.learn2(train_x, train_y)
#model.learn2(trainX2, trainY2)

observations, actions = loadRobotData("Dataset/dataset_v01.npz")

observations = observations[::10, ::2, :]
actions = actions[::10, ::2, :]

model = SKIDynamicsLearner(state_dims=9,
                           action_dims=3,
                           learningRate=0.1,
                           trainingIterations=1000)

model.learn(observations, actions)
