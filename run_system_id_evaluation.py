import sys
import ipdb
import traceback
import numpy as np

import os



def create_evaluation_command(experiment_name,
                              horizon_length,
                              history_length,
                              identification_method,
                              dataset_path,
                              training_set,
                              validation_set,
                              iid_test_set,
                              transfer_test_sets,
                              ouput_error_path):
    # if isinstance(transfer_test_sets, basestring):
    #     transfer_test_sets_paths = dataset_path + transfer_test_sets
    # else:
    transfer_test_sets_paths = \
        [dataset_path + transfer_test_sets[i]
         for i in range(len(transfer_test_sets))]
    transfer_test_sets_paths = ' '.join(transfer_test_sets_paths)

    error_filename = 'system_id__' + experiment_name + \
                     '__horizon_' + str(horizon_length).zfill(4) + \
                     '__history_' + str(history_length).zfill(2) + \
                     '__identification_method_' + identification_method + '.npz'

    evaluation_command = \
        'python -m DL.evaluation.evaluation ' \
        '--method system_id ' \
        ' --training_data ' + dataset_path + training_set + \
        ' --validation_data ' + dataset_path + validation_set + \
        ' --iid_test_data ' + dataset_path + iid_test_set + \
        ' --transfer_test_data ' + transfer_test_sets_paths + \
        ' --output_errors ' + ouput_error_path + error_filename + \
        ' --prediction_horizon ' + str(horizon_length) + \
        ' --history_length ' + str(history_length) + \
        ' --identification_method ' + identification_method + \
        ' --verbose'

    return evaluation_command


if __name__ == "__main__":
    # run(parser=argparse.ArgumentParser(description=__doc__))

    import ipdb
    import traceback

    try:
        dataset_path = '/agbs/dynlearning/Data/'

        for identification_method in ['ls']: #['ls-lmi', 'ls', 'cad', 'constrained_ls']:
            for horizon_length in [10]:
                for history_length in [1]:
                    evaluation_command = \
                    create_evaluation_command(
                        experiment_name='sine',
                        horizon_length=horizon_length,
                        history_length=history_length,
                        identification_method=identification_method,
                        dataset_path='/agbs/dynlearning/Data/',
                        training_set='dataset_v06_sines_training.npz',
                        validation_set='dataset_v06_sines_validation.npz',
                        iid_test_set='dataset_v06_sines_test_iid.npz',
                        transfer_test_sets=['dataset_v06_sines_test_transfer_1.npz',
                                            'dataset_v06_sines_test_transfer_2.npz',
                                            'dataset_v06_sines_test_transfer_3.npz'],
                        ouput_error_path=
                        '/agbs/dynlearning/Errors/new_datasets/SinePD/system_id/')

                    print(evaluation_command)

                    os.system('gnome-terminal -e ' + "'" + evaluation_command +"'")

        # for identification_method in ['ls', 'cad']:  # ['ls', 'cad', 'constrained_ls']:
        #     for horizon_length in [1, 10]:  # [1000, 100, 10, 1]:
        #         for history_length in [10, 1]:
        #             evaluation_command = \
        #                 create_evaluation_command(
        #                     experiment_name='gp',
        #                     horizon_length=horizon_length,
        #                     history_length=history_length,
        #                     identification_method=identification_method,
        #                     dataset_path='/agbs/dynlearning/Data/',
        #                     training_set='dataset_v06_GPs_training.npz',
        #                     validation_set='dataset_v06_GPs_validation.npz',
        #                     iid_test_set='dataset_v06_GPs_test_iid.npz',
        #                     transfer_test_sets=[
        #                         'dataset_v06_GPs_test_transfer.npz'],
        #                     ouput_error_path=
        #                     '/agbs/dynlearning/Errors/new_datasets/GP/system_id/')
        #
        #             os.system('gnome-terminal -e ' + "'" + evaluation_command + "'")




    except:
        traceback.print_exc(sys.stdout)
        _, _, tb = sys.exc_info()
        ipdb.post_mortem(tb)
