from scipy.ndimage import gaussian_filter1d

import matplotlib.pyplot as plt

import pinocchio


from DL.methods.system_id import *


def test_sys_id_simulated_torques():
    robot = Robot()
    test_regressor_matrix(robot)

    # create dataset with simulated torques ------------------------------------
    data = load_data()
    compute_accelerations(data, dt=0.001)

    for key in data.keys():
        data[key] = data[key][:, 10000: 10200]

    data['torque'] = [[robot.inverse_dynamics(
        angle=data['angle'][trajectory_idx, t],
        velocity=data['velocity'][trajectory_idx, t],
        acceleration=data['acceleration'][trajectory_idx, t])
        for t in range(data['angle'].shape[1])]
        for trajectory_idx in range(data['angle'].shape[0])]
    data['torque'] = np.array(data['torque']).squeeze()

    # the usual preprocessing --------------------------------------------------
    data = preprocess_data(data=data,
                           desired_n_data_points=10000,
                           smoothing_sigma=None)

    # identify -----------------------------------------------------------------
    sys_id(robot=robot,
           angle=data['angle'],
           velocity=data['velocity'],
           acceleration=data['acceleration'],
           torque=data['torque'])

    assert (rmse_sequential(robot=robot,
                            angle=data['angle'],
                            velocity=data['velocity'],
                            acceleration=data['acceleration'],
                            torque=data['torque']) < 1e-10)


def test_sys_id_visually():
    assert args.visualizer
    robot = Robot(visualizer=args.visualizer)
    robot.simulate(dt=0.001,
                   n_steps=1000,
                   torque=[0.1, 0.1, 0.1],
                   initial_angle=[1, 1, 1],
                   mask=[1, 1, 1])

    # robot.simulate(dt=0.001, n_steps=10000)

    data = load_data()
    compute_accelerations(data, dt=0.001)

    # # plot ---------------------------------------------------------------------
    # for key in data.keys():
    #     data[key] = gaussian_filter1d(data[key],
    #                                   sigma=3,
    #                                   axis=1)
    #
    #
    # dim = 2
    # sample = 50
    # data['velocity'] /= 20
    # stuff = ['torque', 'acceleration', 'velocity']
    # for key in stuff:
    #     plt.plot(data[key][sample, 1000:2000, dim])
    #
    # plt.legend(stuff)
    #
    # # plt.ylim([-10, 10])
    #
    # plt.show()
    # ipdb.set_trace()

    data = preprocess_data(data=data,
                           desired_n_data_points=10000,
                           smoothing_sigma=1)

    sys_id(robot=robot,
           angle=data['angle'],
           velocity=data['velocity'],
           acceleration=data['acceleration'],
           torque=data['torque'])

    # robot.simulate(dt=0.001, n_steps=10000)


def test_sys_id_lmi():
    robot = Robot()
    # robot.simulate(dt=0.001,
    #                n_steps=1000,
    #                torque=[0.1, 0.1, 0.1],
    #                initial_angle=[1, 1, 1],
    #                mask=[1, 1, 1])

    # robot.simulate(dt=0.001, n_steps=10000)

    data = load_data()
    compute_accelerations(data, dt=0.001)

    # # plot ---------------------------------------------------------------------
    # for key in data.keys():
    #     data[key] = gaussian_filter1d(data[key],
    #                                   sigma=3,
    #                                   axis=1)
    #
    #
    # dim = 2
    # sample = 50
    # data['velocity'] /= 20
    # stuff = ['torque', 'acceleration', 'velocity']
    # for key in stuff:
    #     plt.plot(data[key][sample, 1000:2000, dim])
    #
    # plt.legend(stuff)
    #
    # # plt.ylim([-10, 10])
    #
    # plt.show()
    # ipdb.set_trace()

    data = preprocess_data(data=data,
                           desired_n_data_points=10000,
                           smoothing_sigma=1)

    sys_id_lmi(robot=robot,
               angle=data['angle'],
               velocity=data['velocity'],
               acceleration=data['acceleration'],
               torque=data['torque'])

    # robot.simulate(dt=0.001, n_steps=10000)


def generate_torque(count, length, dim,
                    std_dev, length_scale):
    torque = np.random.normal(0, std_dev, (count, length, 3))
    torque = gaussian_filter1d(torque,
                               sigma=length_scale,
                               axis=0)
    return torque


def generate_data(robot, torque, dt):
    data = {}
    data['torque'] = torque
    data['angle'] = np.empty_like(torque)
    data['velocity'] = np.empty_like(torque)
    data['acceleration'] = np.empty_like(torque)

    for i in range(data['torque'].shape[0]):
        initial_angle = np.array(pinocchio.randomConfiguration(robot.model)).flatten()
        initial_velocity = np.random.uniform(-100, 100, robot.nv)

        initial_angle_copy = initial_angle.copy()
        initial_velocity_copy = initial_velocity.copy()

        (data['angle'][i],
         data['velocity'][i],
         data['acceleration'][i],
         torque) = robot.simulate(dt=dt,
                                  n_steps=data['torque'].shape[1],
                                  torque=data['torque'][i],
                                  initial_angle=initial_angle,
                                  initial_velocity=initial_velocity)

        assert((data['angle'][i, 0] == initial_angle_copy).all())
        assert((data['velocity'][i, 0] == initial_velocity_copy).all())

        assert((torque == data['torque'][i]).all())

    for key in data.keys():
        if(np.isnan(data[key]).any()):
            rollout_index = np.argwhere(np.isnan(data[key]))[0, 0]
            while True:
                ipdb.set_trace()
                robot.play(np.matrix(data['angle'][rollout_index].transpose()), dt)

        assert(not np.isnan(data[key]).any())

    return data


def sys_id_real_data():
    dt = 0.001
    robot = Robot(visualizer='meshcat')
    robot.initViewer(loadModel=True)

    data_file = np.load(
        '/agbs/dynlearning/Data/dataset_v06_sines_training.npz')

    # preprocess data ------------------------------------------------------
    data = dict()
    data['angle'] = data_file['measured_angles']
    data['velocity'] = data_file['measured_velocities']
    data['torque'] = data_file['measured_torques']

    compute_accelerations(data, dt)
    data = preprocess_data(data=data,
                           desired_n_data_points=100000,  # 100000
                           smoothing_sigma=1.0)
    print('Learning with {} points'.format(data['angle'].shape[0]))

    # identify -------------------------------------------------------------
    sys_id(robot=robot,
           angle=data['angle'],
           velocity=data['velocity'],
           acceleration=data['acceleration'],
           torque=data['torque'],
           method_name='lmi')

    params = robot.get_params()
    print(np.array2string(params))

    ipdb.set_trace()


def sys_id_synthetic_data():
    robot_sys_id = Robot(visualizer='meshcat')
    robot_sys_id.initViewer(loadModel=True)

    dt = 0.001
    rollout_count = 30
    rollout_length = 10

    torque = generate_torque(count=rollout_count,
                             length=rollout_length,
                             dim=robot_sys_id.nv,
                             std_dev=3.0,
                             length_scale=3)
    data = generate_data(robot_sys_id, torque, dt)
    for key in data.keys():
        data[key] = np.reshape(data[key], [-1, 3])

    # robot_sys_id.play(np.matrix(data['angle'].transpose()), dt)

    sys_id(robot=robot_sys_id,
           angle=data['angle'],
           velocity=data['velocity'],
           acceleration=data['acceleration'],
           torque=data['torque'],
           method_name='lmi')


if __name__ == '__main__':
    import ipdb
    import traceback

    try:
        sys_id_real_data()
    except:
        extype, value, tb = sys.exc_info()
        traceback.print_exc()
        ipdb.post_mortem(tb)
