import itertools
import gpflow
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf

from DL.utils.data_loading import loadRobotData
from DL.utils.data_loading import unrollTrainingDataStream

class Logger(gpflow.actions.Action):
    def __init__(self, model):
        self.model = model
        self.logf = []

    def run(self, ctx):
        if (ctx.iteration % 100) == 0:
            # Extract likelihood tensor from Tensorflow session
            likelihood = - ctx.session.run(self.model.likelihood_tensor)
            # Append likelihood value to list
            print("Iteration {} loglikelihood {}".format(ctx.iteration,
                    likelihood))
            self.logf.append(likelihood)


if __name__ == "__main__":
    hist_len = 1
    prediction_horizon = 1
    inducing_points = 10
    batch_size = 10
    observations, actions = loadRobotData("Dataset/dataset_v01.npz")
    nseq, _, state_dim = observations.shape
    _, _, action_dim = actions.shape
    input_dim = hist_len * (state_dim + action_dim) + \
            (prediction_horizon - 1) * action_dim
    input_output_shapes = ([state_dim], [input_dim])
    input_output_dtypes = (tf.float64, tf.float64)
    ds = tf.data.Dataset.from_generator(unrollTrainingDataStream,
            input_output_dtypes, input_output_shapes,
            args=(observations, actions, hist_len, prediction_horizon, False,
            True))
    ds = ds.repeat()
    ds = ds.batch(batch_size)
    target_iter, input_iter = ds.make_one_shot_iterator().get_next()
    kernel = gpflow.kernels.RBF(input_dim=input_dim, ARD=True)
    likelihood = gpflow.likelihoods.Gaussian()
    feature = np.random.rand(inducing_points, input_dim)
    model = gpflow.models.SVGP(input_iter, target_iter, kernel, likelihood, feature,
            num_data=nseq)
    adam = gpflow.train.AdamOptimizer().make_optimize_action(model)
    logger = Logger(model)
    actions = [adam, logger]
    loop = gpflow.actions.Loop(actions, stop=1000)()
    model.anchor(model.enquire_session())
    plt.plot(-np.array(logger.logf))
    plt.show()

