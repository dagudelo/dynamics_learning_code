
 # %%
from DL.methods.nn_dynamics_learner import NNDynamicsLearner
from DL.methods.linear_regression_sgd import LinearModelSGD
from DL.utils import loadRobotData, unrollTrainingData
import os
import matplotlib.pyplot as plt
import numpy as np
import json



# %% Train linear model

training_data = "/agbs/dynlearning/Data/dataset_v06_sines_training.npz"
prediction_horizon = 10
history_length = 1
lin_model_10 = LinearModelSGD(history_length, prediction_horizon,
                                  difference_learning=True,
                                  averaging=False,
                                  streaming=True,
                                  settings=None)
training_observations, training_actions = loadRobotData(training_data)
lin_model_10.learn(training_observations, training_actions)


training_data = "/agbs/dynlearning/Data/dataset_v06_sines_training.npz"
prediction_horizon = 100
history_length = 1
lin_model_100 = LinearModelSGD(history_length, prediction_horizon,
                                  difference_learning=True,
                                  averaging=False,
                                  streaming=True,
                                  settings=None)
training_observations, training_actions = loadRobotData(training_data)
lin_model_100.learn(training_observations, training_actions)

# %% Load NN model
path_to_model_folder = "/agbs/dynlearning/Errors/new_datasets/SinePD/NN/prediction_horizon_10_history_length_1_epochs_40"
path_to_model = os.path.join(path_to_model_folder, "model.h5")
path_to_settings = os.path.join(path_to_model_folder, "settings.json")
with open(path_to_settings) as json_file:
    settings = json.load(json_file)
nn_10 = NNDynamicsLearner(
             history_length = 1,
             prediction_horizon=10,
             averaging=False,
             streaming=True,
             model_arch_params=settings["model_arch_params"],
             model_train_params=settings["model_train_params"],
             mode="eval")

nn_10.load_normalization_stats(training_observations, training_actions )
nn_10.load(path_to_model)

# %% Load NN model
path_to_model_folder = "/agbs/dynlearning/Errors/new_datasets/SinePD/NN/prediction_horizon_100_history_length_1_epochs_40"
path_to_model = os.path.join(path_to_model_folder, "model.h5")
path_to_settings = os.path.join(path_to_model_folder, "settings.json")
with open(path_to_settings) as json_file:
    settings = json.load(json_file)
nn_100 = NNDynamicsLearner(
             history_length = 1,
             prediction_horizon=100,
             averaging=False,
             streaming=True,
             model_arch_params=settings["model_arch_params"],
             model_train_params=settings["model_train_params"],
             mode="eval")

nn_100.load_normalization_stats(training_observations, training_actions )
nn_100.load(path_to_model)
# %%
def plot_traj(path_to_data,
              models,
              time_limit,
              title,
              dim=0,
              i=0):
    titles = {'training': 'Training data, trajectory {0}'.format(i+1),
               'validation': 'Validation data, trajectory {0}'.format(i+1),
               'test_iid': 'Test data(i.i.d.), trajectory {0}'.format(i+1),
               'test_transfer_1': 'Transfer data 1, trajectory {0}'.format(i+1),
               'test_transfer_2': 'Transfer data 2, trajectory {0}'.format(i+1),
               'test_transfer_3': 'Transfer data 3, trajectory {0}'.format(i+1)}

    fig = plt.figure(figsize=(10, 5))
    ax = fig.add_subplot(111)
    observations, actions = loadRobotData(path_to_data)
    traj = observations[i:i+1,:, :]
    action = actions[i:i+1, :]
    first_state = traj[:,:1,:]
    first_action = action[:,:1,:]
    future_actions = action[:,1:,:]
    min_ph = min([model.prediction_horizon for model in models])
    N = int(time_limit*int(1000/min_ph))
    t = np.linspace(0, time_limit*1000, N)
    ax.plot(t, traj[0,0:N*min_ph:min_ph, dim], linewidth=3, label="GT")
    labels_dict = {"NN": "NN", "linear-model-SGD": "linear"}
    marker_dict = {10: "-", 100: "o"}
    for model in models:
        assert first_state.shape == (1,1,9)
        predictions = model.predict_recursively(first_state, first_action, future_actions)
        predictions = np.concatenate([first_state, predictions], axis=1)
        N = int(time_limit*int(1000/model.prediction_horizon))
        t = np.linspace(0, time_limit*1000, N)
        ax.plot(t, predictions[0,:N, dim], marker_dict[model.prediction_horizon], label=labels_dict[model.name()]+", prediction_horizon {} $ms$".format(model.prediction_horizon))
    ax.set_title(titles[title], fontsize=18)
    ax.set_xlabel("Time, $ms$", fontsize=18)
    ax.set_ylabel("Angle dimention {0}, [rad]".format(dim+1), fontsize=18)
    plt.legend()
    plt.show()
    return fig



# %%
datasets = ['training',
           'validation',
           'test_iid',
           'test_transfer_1',
           'test_transfer_2',
           'test_transfer_3']

i = 0
dim = 0
for dataset in datasets:
    path_to_data ="/agbs/dynlearning/Data/dataset_v06_sines_{}.npz".format(dataset)
    fig = plot_traj(path_to_data=path_to_data, models=[nn_10, nn_100, lin_model_10, lin_model_100], time_limit=10, title = dataset, i=i, dim=dim)
    fig.savefig("/agbs/dynlearning/Plots/new_datasets/SinePD/trajectory_plots/10_sec_hor_10_100_dim_{2}_traj_{0}_{1}.pdf".format(i, dataset,dim))
